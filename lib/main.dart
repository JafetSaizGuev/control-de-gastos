import 'package:controldegastos/Constants.dart';
import 'package:controldegastos/Vistas/AgregarView.dart';
import 'package:controldegastos/Vistas/ConfiguracionView.dart';
import 'package:controldegastos/Vistas/GraficoView.dart';
import 'package:controldegastos/Vistas/MesesView.dart';
import 'package:controldegastos/Vistas/PrincipalView.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      routes: {
        '/principal':(context) => PrincipalView(),
      },
      home: Navegador(),
    );
  }
}

class Navegador extends StatefulWidget {

  @override
  _NavegadorState createState() => _NavegadorState();
}

class _NavegadorState extends State<Navegador> {

  int _vista = 0;

  Widget Pantalla(int mostrar){
    switch(mostrar){
      case 0:
        return PrincipalView();
        break;
      case 1:
        return MesView();
        break;
      case 2:
        return AgregarView();
        break;
      case 3:
        return GraficarView();
        break;
      case 4:
        return ConfigurarView();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Pantalla(_vista),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _vista,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home),title: Text('home')),
          BottomNavigationBarItem(icon: Icon(Icons.calendar_today,),title: Text('mes')),
          BottomNavigationBarItem(icon: Icon(Icons.add_circle_outline),title: Text('agregar')),
          BottomNavigationBarItem(icon: Icon(Icons.insert_chart),title: Text('graficas')),
          BottomNavigationBarItem(icon: Icon(Icons.settings),title: Text('settings')),
        ],
        onTap: (int a){
          setState(() {
            _vista = a;
          });
        },
      ),
    );
  }
}
