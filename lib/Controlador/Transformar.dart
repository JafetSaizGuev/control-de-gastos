import 'package:controldegastos/Entidades/Movimientos.dart';
import 'package:controldegastos/Entidades/EstadoCaja.dart';

class Transformar{
  List<EstadoCaja> GenerarEstadoCaja(List<Movimiento> movimientos){
    List<EstadoCaja> _Meses = new List();

    for(int a  = 00; a < 12; a++){
      double Inicial = 0,IngresoTotal = 0,EgresoTotal = 0,Final = 0;
      if(a != 0)
        Inicial = _Meses[a-1].estadoCaja;
      for(int b = 0; b < movimientos.length ; b++){
        if(movimientos[b].Fecha.substring(5,7) == a+1){
          if(movimientos[b].Tipo == 1)
            IngresoTotal += movimientos[b].Monto;
          else
            EgresoTotal += movimientos[b].Monto;
        }
        Final = Inicial + IngresoTotal - EgresoTotal;
      }
      _Meses.add(new EstadoCaja(cajaInicial: Inicial,mes: a+1,ingresoTotal: IngresoTotal,egresoTotal: EgresoTotal,estadoCaja: Final));
    }
    return _Meses;
  }
}