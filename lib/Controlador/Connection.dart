import 'package:controldegastos/Componentes/Data.dart';
import 'package:controldegastos/Entidades/Movimientos.dart';
import 'package:controldegastos/Entidades/EstadoCaja.dart';
import 'package:controldegastos/Controlador/Transformar.dart';
import 'dart:async';

class Connection {

  DataBase _D;

  Connection() {
    _D = new DataBase();
    _D.initDB();
  }

  void SetMovimientos(Movimiento movimiento) {
    _D.InsertMovimiento(movimiento);
  }

  void SetConjuto(String conjunto) {
    _D.InsertConjunto(conjunto);
  }

  Future<List<Conjunto>> GetConjuntos() async {
    List<Conjunto> result;

    try{
      await _D.getConjuntos().then((value) => result = value);
    }
    catch(Exception){
      result = new List();
    }

    return result;
  }

  Future<List<Movimiento>> GetMovimientos(int mes) async{
    List<Movimiento> result;

    try{
      await _D.getMonth(mes).then((value) => result = value);
    }
    catch(Exception){
      result = new List();
    }
    return result;
  }

  Future<List<EstadoCaja>> GetEstadosCaja() async{

    Transformar T = new Transformar();
    List<EstadoCaja> result;
    bool error = true;

    try{
      await _D.getALL().then((value) => result = T.GenerarEstadoCaja(value));
    }
    catch(Exception){
      result = T.GenerarEstadoCaja(new List<Movimiento>());
    }
    return result;
  }

}