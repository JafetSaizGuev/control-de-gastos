import 'package:controldegastos/Constants.dart';
import 'package:flutter/material.dart';
import 'package:controldegastos/Componentes/CardForm.dart';
import 'package:controldegastos/Componentes/Movimientos.dart';
import 'package:controldegastos/Entidades/EstadoCaja.dart';

class EstadoDeCaja extends StatelessWidget {
  EstadoDeCaja({this.cabezal,this.muestra,this.mes,this.data});
  final bool cabezal;
  final String muestra;
  final int mes;
  final Future<List<EstadoCaja>> data;

  Widget Cabezal(bool cabezal,int mes){
    List<String> meses = ['ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JILIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE'];
    if(cabezal){
      return Column(
        children: <Widget>[
          Container(
            child: CardForm(
              color: kAzul,
              child: Row(
                children: <Widget>[
                  Expanded(child: Text(muestra,style: kTitle, textAlign: TextAlign.center)),
                  if(mes != null)
                  Expanded(child: Text(meses[mes],style: kTitle,textAlign: TextAlign.center,)),
                ],
              ),
            ),
          ),
          SizedBox(height: 5,)
        ],
      );
    }
    else{
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: data,
      builder: (context,AsyncSnapshot<List<EstadoCaja>> snapshot){
        switch(snapshot.connectionState){
          case ConnectionState.waiting:
            return Column(children: [
              Cabezal(cabezal,mes),
              Expanded(child: Center(child: CircularProgressIndicator(backgroundColor: kAzul,),)),
            ],);
            break;
          case ConnectionState.done:
            return Column(
              children: <Widget>[
                Cabezal(cabezal,mes),
                Row(
                  children: <Widget>[
                    kDividerV,
                    Expanded(child: Text('Saldo Inicial',style: kText)),
                    Expanded(child: Text('\$ '+ snapshot.data[mes].cajaInicial.toString(),style: kText,textAlign: TextAlign.center,)),
                  ],
                ),
                Row(
                  children: <Widget>[
                    kDividerV,
                    Expanded(child: Text('Total de Ingresos',style: kText,)),
                    Expanded(child: Text('\$ '+ snapshot.data[mes].ingresoTotal.toString(),style: kText,textAlign: TextAlign.center,)),
                  ],
                ),
                Row(
                  children: <Widget>[
                    kDividerV,
                    Expanded(child: Text('Total de Egresos',style: kText,)),
                    Expanded(child: Text('\$ '+ snapshot.data[mes].egresoTotal.toString(),style: kText,textAlign: TextAlign.center,)),
                  ],
                ),
                kDividerH,
                Row(
                  children: <Widget>[
                    kDividerV,
                    Expanded(child: Text('Estado de Caja',style: kText,)),
                    Expanded(child: Text('\$ '+ snapshot.data[mes].estadoCaja.toString(),style: kText,textAlign: TextAlign.center,)),
                  ],
                ),
              ],
            );
            break;
          default:
            return Column(
              children: [
                Cabezal(cabezal, mes),
                Expanded(child: Center(child: Text('no data',style: kTextError,),)),
              ],
            );
            break;
        }
    });
  }
}


class FlujoCaja extends StatelessWidget {

  FlujoCaja({this.tipo,this.Mes,this.datos,this.tipe});
  final int Mes;
  final bool tipe;
  final String tipo;
  final Future datos;

  List<Widget> movimientos(){}
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CardForm(
            color: (tipe) ? kVerde : kRojo,
            child: Container(child: Text(tipo,style: kTitle,textAlign: TextAlign.center,),width: double.infinity,height: 30,alignment: Alignment.center,),
        ),
        Padding(
          padding: EdgeInsets.only(top: 40),
          child: FutureBuilder(
            future: datos,
            builder: (BuildContext context,AsyncSnapshot snapshot){
              switch(snapshot.connectionState) {
                case ConnectionState.waiting:
                  return Center(
                    child: CircularProgressIndicator(backgroundColor: kAzul,),);
                  break;
                case ConnectionState.done:
                  return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int i) {
                        return WMovimientos(movimiento: snapshot.data[i].Concepto,
                          fecha: snapshot.data[i].Fecha,
                          monto: snapshot.data[i].Monto,);
                      });
                  break;
                default:
                  return Center(
                      child: Text('no movements', style: kTextError));
                  break;
              }
            },
          ),
        ),
      ],
    );
  }
}