import 'package:flutter/material.dart';

class MyAppBar extends StatefulWidget {
  MyAppBar({this.titulo,this.actions,@required this.cuerpo});

  final String titulo;
  final Widget cuerpo;
  final List<Widget> actions;

  @override
  _MyAppBarState createState() => _MyAppBarState();
}

class _MyAppBarState extends State<MyAppBar> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: (widget.titulo != null) ? AppBar(title: Text(widget.titulo),actions: widget.actions,) : null,
        body: widget.cuerpo,
    );
  }
}
