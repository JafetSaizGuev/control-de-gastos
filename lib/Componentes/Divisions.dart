import 'package:flutter/material.dart';

class Division extends StatelessWidget {
  Division({@required this.child1,@required this.child2,@required this.child3});
  final Widget child1;
  final Widget child2;
  final Widget child3;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 2,
          child: child1,
        ),
        Expanded(
          flex: 2,
          child: child2,
        ),
        Expanded(
          flex: 2,
          child: child3,
        )
      ],
    );
  }
}

class BOTTOMDivision extends StatelessWidget {
  BOTTOMDivision({@required this.child1,@required this.child2,@required this.child3});
  final Widget child1;
  final Widget child2;
  final Widget child3;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 6,
          child: child1,
        ),
        Expanded(
          flex: 7,
          child: child2,
        ),
        Expanded(
          flex: 7,
          child: child3,
        ),
      ],
    );
  }
}
class TwoDivision extends StatelessWidget {
  TwoDivision({@required this.child1,@required this.child2});
  final Widget child1;
  final Widget child2;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: child1,
        ),
        Expanded(
          flex: 1,
          child: child2,
        ),
      ],
    );
  }
}