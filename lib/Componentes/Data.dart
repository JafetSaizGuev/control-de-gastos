import 'package:sqflite/sqflite.dart';
import 'package:controldegastos/Entidades/Movimientos.dart';
import 'dart:async';

class DataBase {
  Database _DB;

  initDB() async {
    _DB = await openDatabase('my_data.db', version: 1,
        onCreate: (Database DB, int version) {
          DB.execute('create table conjuntos(Tipos text PRIMARY KEY);CREATE TABLE movimientos (id INTEGER PRIMARY KEY AUTOINCREMENT,Descripcion text NOT NULL,Tipo boolean NOT NULL,Conjunto text,Monto double NOT NULL,fecha date NOT NULL,FOREIGN KEY(Conjunto)REFERENCES Conjuntos(Tipos));');
        }
    );
  }

  InsertMovimiento(Movimiento data) async {
    _DB.rawInsert("insert into movimientos values(null,'"+data.Descripcion+"',"+data.Tipo.toString()+",'"+data.Conjunto+"',"+data.Monto.toString()+",'"+data.Fecha+"')");
  }
  InsertConjunto(String conjunto) async {
    _DB.rawInsert('insert into conjuntos values ("$conjunto")',);
  }

  Future<List<Conjunto>> getConjuntos() async {
    List<Map<String, dynamic>> result = await _DB.query('conjuntos');
    return result.map((map) => Conjunto.fromMap(map));
  }

  Future<List<Movimiento>> getALL() async {
    List<Map<String, dynamic>> result = await _DB.query('movimientos');
    return result.map((map) => Movimiento.fromMap(map));
  }

  Future<List<Movimiento>> getMonth(int mes) async{
    List<Map<String,dynamic>> result = await _DB.rawQuery('SELECT * from movimientos WHERE strftime("%m",fecha) = strftime("$mes");');
    return result.map((map) => Movimiento.fromMap(map));
  }

}

/*
create table Conjuntos(
	Tipos text PRIMARY KEY
);

CREATE TABLE movimientos (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  Descripcion text NOT NULL,
  Tipo boolean NOT NULL,
  Conjunto text,
  Monto double NOT NULL,
  fecha date NOT NULL,
  FOREIGN KEY(Conjunto)REFERENCES Conjuntos(Tipos)
);
/*INSERT INTO Conjuntos VALUES("algo");
INSERT INTO movimientos VALUES(null,"dos",1,"trasporte",420.00,"2020-08-29");*/
select * from Conjuntos;
select * from movimientos;
select * from movimientos where Tipo = 0;
select * from movimientos where Tipo = 1;
select * from movimientos where strftime("%m",fecha) = strftime('08');
 */