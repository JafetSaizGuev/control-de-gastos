import 'package:controldegastos/Constants.dart';
import 'package:flutter/material.dart';

class WMovimientos extends StatelessWidget {
  WMovimientos({this.movimiento,this.fecha,this.monto});
  final String movimiento;
  final String fecha;
  final double monto;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.grey,borderRadius: BorderRadius.circular(13)),
      margin: EdgeInsets.only(top: 2,bottom: 2,left: 10,right: 10),
      child: Container(
        decoration: BoxDecoration(color: Colors.black,borderRadius: BorderRadius.circular(13)),
        margin: EdgeInsets.all(2),
        padding: EdgeInsets.only(left: 8,right: 8,top: 2,bottom: 2),
        child: Row(
          children: <Widget>[
            Expanded(child: Text(movimiento,style: kSmallText,textAlign: TextAlign.center,)),
            Expanded(child: Text(fecha,style: kSmallText,textAlign: TextAlign.center,)),
            Expanded(child: Text('\$ $monto',style: kSmallText,textAlign: TextAlign.center,)),
          ],
        ),
      ),
    );
  }
}