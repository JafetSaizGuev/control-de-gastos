import 'package:flutter/material.dart';

class CardForm extends StatelessWidget {
  CardForm({@required this.child,this.onTap,this.color = Colors.black26});
  final Widget child;
  final Function onTap;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10),
        ),
        margin: EdgeInsets.all(5),
        child: child,
      ),
      onTap: onTap,
    );
  }
}