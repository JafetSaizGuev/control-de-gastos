class Movimiento{

  Movimiento({this.id,this.Tipo,this.Fecha,this.Descripcion,this.Monto,this.Conjunto});

  Map<String,dynamic> toMap(){
    return{
      "id": id,
      "descripcion": Descripcion,
      "monto": Monto,
      "fecha": Fecha,
      "tipo": Tipo,
      "conjunto": Conjunto,
    };
  }

  Movimiento.fromMap(Map<String,dynamic> Map){
    id = Map['id'];
    Descripcion = Map['Descripcion'];
    Monto = Map['Monto'];
    Fecha = Map['Fecha'];
    Tipo = Map['TipoMovimiento'];
    Conjunto = Map['Conjunto'];
  }

  int id;
  String Descripcion;
  String Conjunto;
  String Fecha;
  int Tipo;
  double Monto;

}

class Conjunto{
  Conjunto({this.nombre});

  String nombre;

  Conjunto.fromMap(Map<String,dynamic> Map){
    nombre = Map['Tipos'];
  }
}