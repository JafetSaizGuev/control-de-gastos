import 'package:flutter/material.dart';
import 'package:controldegastos/Constants.dart';
import 'package:controldegastos/Componentes/CardForm.dart';
import 'package:controldegastos/Componentes/MyAppBar.dart';

class AgregarView extends StatefulWidget {
  @override
  _AgregarViewState createState() => _AgregarViewState();
}

class _AgregarViewState extends State<AgregarView> {

  String _fecha;
  double _slider = 0.5;

  String _hoy(){
    DateTime ahora = new DateTime.now();
    return ahora.year.toString() + "-" + ahora.month.toString() + "-" + ahora.day.toString();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fecha = _hoy();
  }

  @override
  Widget build(BuildContext context) {
    return MyAppBar(cuerpo:
      Center(child: CardForm(
        child: Container(
          alignment: Alignment.center,
          height: 400,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 40),
                //--------------------------------DESCRIPCION-------------
                child: TextField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.text_fields),
                    labelText: 'Descripcion',
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 40),
                //---------------------------------CANTIDAD----------------
                child: TextField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.attach_money),
                    labelText: 'Cantidad',
                  ),
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 29,right: 40),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      //-----------------------------FECHA------------------
                      child: IconButton(icon: Icon(Icons.alarm), onPressed: (){
                        showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2010),
                            lastDate: DateTime(2030),
                        ).then((date) {
                          setState(() {
                            String nueva = date.year.toString() + "-" + date.month.toString() + "-" + date.day.toString();
                            _fecha = nueva;
                          });
                        });
                      }),
                    ),
                    Expanded(
                      flex: 5,
                      child: TextField(
                        enabled: false,
                        controller: TextEditingController(text: _fecha),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 40),
                //---------------------------------CONJUNTO--------------
                child: DropdownButton(
                  iconSize: 48,
                  isExpanded: true,
                  items: [
                    DropdownMenuItem(child: Text('esta :p')),
                  ],
                  onChanged: (dynamic a){}),
              ),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Column(
                      children: [
                        //--------------------------TIPO----------------
                        Slider(
                          max: 1,
                          min: 0,
                          divisions: 1,
                          activeColor: kAzul,
                          value: _slider,
                          onChanged: (value){setState(() {
                            _slider = value;
                          });
                          }
                        ),
                        Row(children: [
                          Padding(padding: EdgeInsets.only(right: 30),child: Text('ENTRADA',style: kTextError,)),
                          Padding(padding: EdgeInsets.only(left: 30),child: Text('SALIDA',style: kTextError,)),
                        ],),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(right: 30),
                      alignment: Alignment.centerRight,
                        //-----------------------------ENVIAR------------
                        child: IconButton(
                            iconSize: 50,
                            color: kAzul,
                            icon: Icon(Icons.check_circle),
                            onPressed: (){})),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),),
    );
  }
}