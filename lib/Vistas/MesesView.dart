import 'package:controldegastos/Controlador/Connection.dart';
import 'package:flutter/material.dart';
import 'package:controldegastos/Constants.dart';
import 'package:controldegastos/Entidades/EstadoCaja.dart';
import 'package:controldegastos/Componentes/Divisions.dart';
import 'package:controldegastos/Componentes/CardForm.dart';
import 'package:controldegastos/Componentes/MyAppBar.dart';
import 'package:controldegastos/Componentes/MyTables.dart';

class MesView extends StatefulWidget {
  @override
  _MesViewState createState() => _MesViewState();
}

class _MesViewState extends State<MesView> {

  int _value;

  Future<List<EstadoCaja>> ObtenerEstados() async{
    Connection C = new Connection();
    return await C.GetEstadosCaja();
  }

  int _Meses() {
    DateTime ahora = new DateTime.now();
    return ahora.month - 1;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _value = _Meses();
  }

  @override
  Widget build(BuildContext context) {
    const List<String> _meses = ["ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"];
    const List<String> _mes = ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"];
    return MyAppBar(
        titulo: _meses[_value],
        actions: [
          DropdownButton(
            value: _value,
            items: [
              for(int a = 0;a < _mes.length; a++)
                DropdownMenuItem(child: Text(_mes[a]),value: a,)
            ],
            onChanged: (var b){
              setState(() {
                _value = b;
              });
            }),
        ],
        cuerpo: BOTTOMDivision(
            child1: CardForm(child: EstadoDeCaja(
              cabezal: false,
              mes: _value,
              data: ObtenerEstados(),
            ),),
            child2: CardForm(child: FlujoCaja(
              tipo: 'ENTRADAS',
              tipe: true,
            )),
            child3: CardForm(child: FlujoCaja(
              tipo: 'SALIDAS',
              tipe: false,
            )),
        ),
    );

  }
}