import 'dart:math';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:controldegastos/Constants.dart';
import 'package:controldegastos/Componentes/Divisions.dart';
import 'package:controldegastos/Componentes/CardForm.dart';
import 'package:controldegastos/Componentes/MyAppBar.dart';
import 'package:controldegastos/Componentes/MyTables.dart';
import 'package:controldegastos/Entidades/EstadoCaja.dart';
import 'package:controldegastos/Controlador/Connection.dart';

class PrincipalView extends StatefulWidget {
  @override
  _PrincipalViewState createState() => _PrincipalViewState();
}

class _PrincipalViewState extends State<PrincipalView> {

  int _month;

  int _mes(){
    DateTime d = DateTime.now();
    return d.month;
  }

  Future<List<EstadoCaja>> ObtenerData() async{
    Connection C = new Connection();
    return await C.GetEstadosCaja();
  }
  @override
  void initState() {
    super.initState();
    _month = _mes()-1;
  }

  @override
  Widget build(BuildContext context) {
    const List<String> _meses = ["ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"];
    return SafeArea(
      child: MyAppBar(cuerpo: Division(
          child1: CardForm(child: Container(
            child: Row(children: [
              for(int a = 0; a < 12; a++)
              Expanded(child: Container(width: double.infinity,decoration: BoxDecoration(border: Border.all(width: 2)),child: GestureDetector(
                onTap: (){
                  setState(() {
                    _month = a;
                  });
                },
              ),)),
            ],),
          )),
          child2: CardForm(child: EstadoDeCaja(
            cabezal: true,
            muestra: 'REAL',
            data: ObtenerData(),
            mes: _month,
          ),),
          child3: CardForm(child: EstadoDeCaja(
            cabezal: true,
            muestra: 'ESPERADO',
          ),
        ),
      ),),
    );
  }
}
