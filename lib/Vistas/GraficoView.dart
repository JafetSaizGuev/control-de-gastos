import 'package:flutter/material.dart';
import 'package:controldegastos/Constants.dart';
import 'package:controldegastos/Componentes/Divisions.dart';
import 'package:controldegastos/Componentes/CardForm.dart';
import 'package:controldegastos/Componentes/MyAppBar.dart';

class GraficarView extends StatefulWidget {
  @override
  _GraficarViewState createState() => _GraficarViewState();
}

class _GraficarViewState extends State<GraficarView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: MyAppBar(cuerpo: TwoDivision(
        child1: CardForm(child: Container(),),
        child2: CardForm(child: Container(),),
      )),
    );
  }
}