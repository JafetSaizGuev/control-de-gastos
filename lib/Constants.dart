import 'package:flutter/material.dart';

const kTitle = TextStyle(fontWeight: FontWeight.bold,fontSize: 20);
const kSmallText = TextStyle(fontSize: 18);
const kText = TextStyle(fontSize: 20);
const kNumber = TextStyle(color: Colors.grey,fontSize: 10);
const kTextError = TextStyle(fontSize: 16,color: Colors.grey);

const kDividerH = Divider(color: Colors.white,thickness: 1.5,indent: 40,endIndent: 40,);
const kDividerV = SizedBox(width: 20,height: 35,);

const kRojo = Color(0xFF522831);
const kAzul = Color(0x4400CCFF);
const kVerde = Color(0xFF2E502C);