# control de gastos 

Una nueva aplicación creada con Flutter.
Se trata de un control de gastos con barra de navegación y base de datos local la cual gráfica y muestra los movimientos realizados mes a mes.
Manteniendo una arquitectura MVC. Agregando los paquetes externos SQFlite y flutter_sparckline.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
